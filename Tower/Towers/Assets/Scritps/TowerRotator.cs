﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRotator : MonoBehaviour
{
    [SerializeField]
    bool moving = false;//delay 6s after spawn to move

    GameObject bulletPrefab;
    Transform bulletSpawn;

    [SerializeField]
    int shootCount = 0; // numer of shoots

    float timePassed = 0f;

    float spawnTime = 0f;
    private void Awake()
    {
        bulletPrefab = Resources.Load<GameObject>("bullet");    //load prefab
        bulletSpawn = transform.GetChild(0);                    //get bulletspawn transform
    }
    void Start()
    {
        Vector3 temp = transform.position;
        temp.z = 0f;
        transform.position = temp;
        StartCoroutine(RandomRotation(0.5f));
        spawnTime = Time.time;
        timePassed = spawnTime;
    }

    // Update is called once per frame
    void Update()
    {

        if (timePassed < spawnTime + 6f)
        {
            timePassed = Time.time;
        }
        else
        {
            moving = true;

        }

        if (moving && shootCount <= 12)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (!moving || shootCount > 12)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);
        bullet.GetComponent<BulletLogic>().SetDirection(bulletSpawn.position - transform.position);
    }
    IEnumerator RandomRotation(float duration)
    {
        while (!moving)
        {
            yield return null;
        }

        float startRotation = transform.eulerAngles.z;
        float endRotation = startRotation + Random.Range(15f, 46f);
        float t = 0.0f;
        while (t < duration)
        {
            t += Time.deltaTime;
            float zRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, zRotation);
            yield return null;
        }
        if (shootCount <= 12)
        {
            StartCoroutine(RandomRotation(0.5f));
            Shoot();
            shootCount++;
        }
        else
        {
            moving = false;
            yield return null;
        }
    }
    public void Restart()
    {
        shootCount = 0;
        moving = true;
    }
    public void ClearData()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
        StopAllCoroutines();
        spawnTime = Time.time;
        timePassed = spawnTime;
        moving = false;
        shootCount = 0;
        StartCoroutine(RandomRotation(0.5f));

    }
}
