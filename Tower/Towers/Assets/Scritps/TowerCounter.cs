﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerCounter : MonoBehaviour
{
    public static TowerCounter Instance;

    [SerializeField]
    Text towerCounterText;

    [SerializeField]
    int towerCount = 1;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        towerCounterText = GameObject.Find("towerCounterText").GetComponent<Text>();
    }
    private void Update()
    {
        towerCounterText.text = $"Tower: {towerCount}";
        if(towerCount == 100)
        {
            GameObject[] Towers = GameObject.FindGameObjectsWithTag("tower");

            foreach(GameObject Tower in Towers)
            {
                Tower.GetComponent<TowerRotator>().Restart();
            }
        }
    }
    public void Add()
    {
        towerCount++;
    }
    public void Remove()
    {
        towerCount--;
    }
    public int CurrentValue()
    {
        return towerCount;
    }
}
