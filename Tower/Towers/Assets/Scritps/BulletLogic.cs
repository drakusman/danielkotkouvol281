﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    Vector2 direction;
    Vector2 start;
    //random distance 1-4
    int distance;

    public void SetDirection(Vector2 direction)
    {
        this.direction = direction;
    }
    private void Start()
    {

        start = transform.position;
        distance = Random.Range(1, 5);
        StartCoroutine(MoveToPoint());
    }
    IEnumerator MoveToPoint()
    {
        while (Vector3.Distance(start, transform.position) < distance)
        {
            transform.Translate(direction.normalized * Time.deltaTime * 4f);
            yield return null;
        }
        SpawnNewTower();
    }
    void SpawnNewTower()
    {
        if (TowerCounter.Instance.CurrentValue() < 100)
        {
            TowerCounter.Instance.Add();
            GameObject newTower = ObjectPool.SharedInstance.GetPooledObject();
            newTower.SetActive(true);
            newTower.transform.position = transform.position;
        }
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("tower"))
        {
            TowerCounter.Instance.Remove();
            ObjectPool.SharedInstance.returnPooledObject(other.gameObject);
            Destroy(gameObject);
        }

    }
}
